'use strict'
const path = require('path')
module.exports = {
    // 开发过程中使用的配置
  dev: {
    // webpack编译输出的二级文件夹
    assetsSubDirectory: 'static',
    // webpack编译输出的发布路径
    assetsPublicPath: '',
    // 请求代理表，在这里可以配置特定的请求代理到对应的API接口
    proxyTable: {
        '/':{
            target:'http://192.168.1.100',
            changeOrigin:true,
            pathRewrite:{
                '^/api':''
            }
        }
    },
    host: 'localhost', 
     // dev-server监听的端口
    port: 8080,
    // 启动dev-server之后自动打开浏览器
    autoOpenBrowser: false,
    errorOverlay: true,
    notifyOnErrors: true,
    poll: false,
    useEslint: true,
    showEslintErrorsInOverlay: false,
    devtool: 'cheap-module-eval-source-map',
    cacheBusting: true,
    // 是否开启 cssSourceMap
    cssSourceMap: true
  },
  // 构建产品时使用的配置
  build: {
    // 编译输入的index.html文件
    index: path.resolve(__dirname, '../dist/index.html'),
    // 页面入口文件
    // cuxiao: path.resolve(__dirname, '../dist/cuxiao.html'),
    // webpack输出的目标文件夹路径
    assetsRoot: path.resolve(__dirname, '../dist'),
    // webpack编译输出的二级文件夹
    assetsSubDirectory: 'static',
    // webpack编译输出的发布路径
    assetsPublicPath: '',
     // 使用SourceMap
    productionSourceMap: false,
    devtool: '#source-map',
    // 默认不打开开启gzip模式
    productionGzip: false,
    // gzip模式下需要压缩的文件的扩展名
    productionGzipExtensions: ['js', 'css'],
    bundleAnalyzerReport: process.env.npm_config_report
  }
}
