// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import Axios from 'axios';
import qs from 'qs';
import Element from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import "./assets/all.less";


Axios.defaults.timeout = 10000;                        //响应时间
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';           //配置请求头
// axios.defaults.baseURL = 'http://spsj.xn62x.cn';   //配置接口地址
// axios.defaults.baseURL = 'http://192.168.1.100';   //配置接口地址
// 添加一个响应拦截器
Axios.interceptors.request.use((config) => {
  //在发送请求之前做某件事
  // 判断是否登录
  	let cur_id = "cur_id",
  		sign = "sign";
    if (!cur_id||!sign) {
      localStorage.clear();
      window.location.href = "index.html";
    };
    if(config.method  === 'post'){
        config.data = qs.stringify(config.data);
    }
    return config;
},(error) =>{
     _.toast("错误的传参", 'fail');
    return Promise.reject(error);
});

Vue.prototype.$imgurl = "http://192.168.1.100/";


Vue.config.productionTip = false
Vue.use(Element);
Vue.prototype.$http= Axios;
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
